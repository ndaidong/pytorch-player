#!/usr/bin/env python3

import os

from PIL import Image


INPUT_DIR = os.getenv('INPUT_DIR', '/workspace/ml/datasets/emotions/')
OUTPUT_DIR = os.getenv('OUTPUT_DIR', '/workspace/ml/datasets/emotious/')


def init():
    if not os.path.exists(OUTPUT_DIR):
        os.system(f'mkdir -p {OUTPUT_DIR}')

    for dname in os.listdir(INPUT_DIR):
        label_dir = f'{OUTPUT_DIR}/{dname}'
        if not os.path.exists(label_dir):
            print(f'Create directory "{label_dir}"')
            os.system(f'mkdir -p {label_dir}')
        subpath = INPUT_DIR + dname
        for subdname in os.listdir(subpath):
            print('  ' + subdname)
            subdir = subpath + '/' + subdname
            for fname in os.listdir(subdir):
                fsrc = f'{subdir}/{fname}'
                fdes = f'{label_dir}/{fname}'
                if not os.path.exists(fdes):
                    print(f'Copy {fname} to label dir')
                    # os.system(f'cp {fsrc} {fdes}')
                    png_im = Image.open(fsrc)
                    jpg_im = png_im.convert('RGB')
                    jpg_im.save(fdes.replace('.png', '.jpg'))


if __name__ == '__main__':
    init()
