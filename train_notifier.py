#!/usr/bin/env python3

import os
import time
from datetime import datetime
from copy import deepcopy
from functools import reduce

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import pandas as pd
import matplotlib.pyplot as plt
from bella import pipe

DATA_FILE = os.getenv('DATA_FILE', 'event_data.csv')
DATA_DIR = os.getenv('DATA_DIR', '/workspace/ml/datasets')
OUTPUT_DIR = os.getenv('OUTPUT_DIR', '/workspace/ml/output')
MODEL_NAME = os.getenv('MODEL_NAME', 'notifier')


class SampleSet(Dataset):

    def __init__(self, features, labels, transform=None):
        self.features = features
        self.labels = labels
        self.transform = transform

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        feature = self.features[idx]
        label = self.labels[idx]

        if self.transform:
            feature, label = self.transform(feature, label)

        return feature, label

    def __len__(self):
        return len(self.features)


class NeuralNet(nn.Module):

    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(3, 8)
        self.fc2 = nn.Linear(8, 16)
        self.fc3 = nn.Linear(16, 8)
        self.fc4 = nn.Linear(8, 1)

    def forward(self, x):
        x = x.view(-1, self.num_flat_features(x))
        fn = pipe(
            self.fc1,
            self.fc2,
            self.fc3,
            self.fc4,
            torch.sigmoid
        )
        return fn(x)

    def num_flat_features(self, x):
        sizes = x.size()[1:]  # all dimensions except the batch dimension
        return reduce(lambda x, y: x * y, sizes, 1)


def transform(features, label):
    a = torch.tensor(features, dtype=torch.float32)
    b = torch.tensor(label, dtype=torch.float32)
    return (a, b)


def preprocess(df, normolizing=False):
    features = df.iloc[:, 0:3]
    labels = df.iloc[:, 3:]
    if normolizing:
        features = features.apply(
            lambda x: (x-x.mean())/(x.std())
        )
    return features, labels


def get_time():
    return int(time.time())


def train(samples, labels, learning_rate=0.01, epochs=300):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Start training on {device}...')

    train_dataset = SampleSet(samples, labels, transform)
    train_loader = DataLoader(
        train_dataset,
        batch_size=300,
        shuffle=True,
        num_workers=4
    )

    model = NeuralNet()
    model.to(device)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    epoch_list = []
    epoch_loss_list = []
    log_list = []

    best_model_wht = deepcopy(model.state_dict())
    best_loss = 1.0

    start = datetime.fromtimestamp(get_time())

    for epoch in range(epochs):
        epoch_list.append(epoch)
        running_loss = 0.0
        for i, data in enumerate(train_loader):
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)
            labels = labels.view(-1, 1)

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item() * inputs.size(0)

        epoch_loss = running_loss / len(train_dataset)
        epoch_loss_list.append(epoch_loss)

        if epoch_loss < best_loss:
            best_loss = epoch_loss
            best_model_wht = deepcopy(model.state_dict())

        if (epoch + 1) % 50 == 0:
            msg = f'Epoch: {epoch + 1} Loss: {epoch_loss}'
            print(msg)
            log_list.append(msg)

    now = get_time()
    end = datetime.fromtimestamp(now)
    print(f'Training completed in {end - start}')

    mfile = f'{OUTPUT_DIR}/{MODEL_NAME}_checkpoint_{now}.pth'
    torch.save(best_model_wht, mfile)

    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.plot(epoch_list, epoch_loss_list)
    pfile = f'{OUTPUT_DIR}/{MODEL_NAME}_plot_{now}.png'
    plt.savefig(pfile)

    lfile = f'{OUTPUT_DIR}/{MODEL_NAME}_log_{now}.txt'
    with open(lfile, 'w') as fo:
        fo.write('\n'.join(log_list))


def init():
    if not os.path.exists(DATA_DIR):
        print('DATA_DIR is wrong or not set')
        exit()
    data_file = f'{DATA_DIR}/{DATA_FILE}'
    if not os.path.exists(data_file):
        print(f'Could not find out "{data_file}"')
        exit()

    if not os.path.exists(OUTPUT_DIR):
        os.system(f'mkdir -p "{OUTPUT_DIR}"')

    df = pd.read_csv(data_file)
    features, labels = preprocess(df)
    train(features.values, labels.values)


if __name__ == '__main__':
    init()
