#!/usr/bin/env python3

import os
import time
from datetime import datetime
from copy import deepcopy

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision
import torchvision.transforms as transforms

import matplotlib.pyplot as plt


DATA_DIR = os.getenv('DATA_DIR', '/workspace/ml/datasets/')
OUTPUT_DIR = os.getenv('OUTPUT_DIR', '/workspace/ml/output')
MODEL_NAME = os.getenv('MODEL_NAME', 'catdog')


