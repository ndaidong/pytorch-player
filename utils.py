#!/usr/bin/env python3

from time import time

import torch


def get_time():
    return int(time.time())


def get_device():
    return torch.device('cuda' if torch.cuda.is_available() else 'cpu')
