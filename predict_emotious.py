#!/usr/bin/env python3

import sys

import torch
import torchvision.transforms as transforms

from PIL import Image

from train_emotious import NeuralNet


classes = (
    'happy', 'sad', 'serious'
)


def predict(model_path, img_path):
    try:
        model = NeuralNet()
        model.load_state_dict(torch.load(model_path))
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        model.to(device)

        transform = transforms.Compose([
            transforms.Resize((32, 32)),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        im = Image.open(img_path)
        rgb_im = im.convert('RGB')
        img = transform(rgb_im)
        img = img.view(-1, 3, 32, 32)
        img = img.to(device)
        prediction = model(img)
        result = torch.argmax(prediction, dim=1)
        print('*' * 50)
        print(f'model({img_path}) --> {classes[result]}')

    except Exception as err:
        print(err)


def init():
    argv = sys.argv
    if len(argv) == 3:
        predict(argv[1], argv[2])


if __name__ == '__main__':
    init()
