{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# House Prices: Advanced Regression Techniques"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Import necessary dependencies\n",
    "-----------------------------------"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import os\n",
    "import sys\n",
    "sys.path.append('/workspace/ml/RAdam/')\n",
    "from collections import OrderedDict\n",
    "\n",
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim\n",
    "\n",
    "from torch.utils.data import Dataset\n",
    "from torch.utils.data import DataLoader\n",
    "\n",
    "import warnings  \n",
    "with warnings.catch_warnings():  \n",
    "    warnings.filterwarnings(\"ignore\", category=FutureWarning)\n",
    "    import numpy as np\n",
    "    from torch.utils.tensorboard import SummaryWriter\n",
    "\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import radam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Accessing and Reading Data Sets\n",
    "------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience, we already downloaded the data and stored it in the`data` directory.\n",
    "To load the two CSV (Comma Separated Values) files containing training and test data respectively we use Pandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(2919, 79)\n"
     ]
    }
   ],
   "source": [
    "train_data = pd.read_csv('/workspace/ml/house_price_pred/data/train.csv')\n",
    "test_data = pd.read_csv('/workspace/ml/house_price_pred/data/test.csv')\n",
    "all_features = pd.concat((train_data.iloc[:,1:-1], test_data.iloc[:,1:]))\n",
    "print(all_features.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Data Preprocessing\n",
    "------------------------\n",
    "\n",
    "Before we feed it into a deep network, we need toperform some amount of processing.\n",
    "Let’s start with the numerical features. We begin by replacing missing values with the mean.\n",
    "This is a reasonable strategy if features are missing at random.\n",
    "To adjust them to acommon scale, we rescale them to zero mean and unit variance.\n",
    "\n",
    "$$ x \\leftarrow \\frac{x - \\mu}{\\sigma} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The reason for ‘normalizing’ the data is that it brings all features to the same order of magnitude\n",
    "numeric_features = all_features.dtypes[all_features.dtypes != 'object'].index\n",
    "all_features[numeric_features] = all_features[numeric_features].apply(\n",
    "    lambda x: (x-x.mean())/(x.std()))\n",
    "\n",
    "# After standardizing the data all means vanish, hence we can set missing values to 0\n",
    "all_features[numeric_features] = all_features[numeric_features].fillna(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we deal with discrete values. This includes variables such as 'MSZoning'. We replace them by a one-hot encoding in the same manner as how we transformed multiclass classification data into a vector of $0$ and $1$. For instance, 'MSZoning' assumes the values 'RL' and 'RM'. They map into vectors $(1,0)$ and $(0,1)$ respectively. Pandas does this automatically for us."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2919, 331)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Dummy_na=True refers to a missing value being a legal eigenvalue, and\n",
    "# creates an indicative feature for it\n",
    "all_features = pd.get_dummies(all_features, dummy_na=True)\n",
    "all_features.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that this conversion increases the number of features from 79 to 331.\n",
    "\n",
    "Finally, via the `from_numpy` attribute, we can extract the NumPy format from the Pandas dataframe and convert it into PyTorch’s native Tensor representation for training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "num_features: 331\n",
      "num_train: 1460\n",
      "train_features: torch.Size([1460, 331])\n",
      "train_labels: torch.Size([1460, 1])\n",
      "test_features: torch.Size([1459, 331])\n"
     ]
    }
   ],
   "source": [
    "num_train = train_data.shape[0]\n",
    "num_features = all_features.shape[1]\n",
    "train_features = torch.from_numpy(all_features[:num_train].values).float()\n",
    "test_features = torch.from_numpy(all_features[num_train:].values).float()\n",
    "train_labels = torch.from_numpy(train_data.SalePrice.values).view(-1, 1).float()\n",
    "\n",
    "print(f'num_features: {num_features}')\n",
    "print(f'num_train: {num_train}')\n",
    "print(f'train_features: {train_features.size()}')\n",
    "print(f'train_labels: {train_labels.size()}')\n",
    "print(f'test_features: {test_features.size()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "class HousePriceDataset(Dataset):\n",
    "    \n",
    "    def __init__(self, train_features, train_labels, transforms=None):\n",
    "        self.train_features = train_features\n",
    "        self.train_labels = train_labels\n",
    "        self.transforms = transforms\n",
    "\n",
    "    def __len__(self):\n",
    "        return self.train_features.size(0)\n",
    "\n",
    "    def __getitem__(self, idx):\n",
    "        if torch.is_tensor(idx):\n",
    "            idx = idx.tolist()\n",
    "\n",
    "        features = self.train_features[idx]\n",
    "        label = self.train_labels[idx]\n",
    "\n",
    "        if self.transforms:\n",
    "            features = self.transforms(features)\n",
    "\n",
    "        return features, label"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Training\n",
    "---------------"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loss Function\n",
    "criterion = nn.MSELoss()\n",
    "\n",
    "# Define the network\n",
    "def get_net():\n",
    "    net = nn.Sequential(\n",
    "        nn.Linear(num_features, 1)\n",
    "    )\n",
    "    return net"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def log_rmse(net, features, labels):\n",
    "    # To further stabilize the value when the logarithm is taken, set the\n",
    "    # value less than 1 as 1\n",
    "    clipped_preds = torch.clamp(net(features), min=1, max=float('inf'))\n",
    "    rmse = torch.sqrt(criterion(clipped_preds.log(), labels.log()))\n",
    "    return rmse.item()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(net, train_features, train_labels, test_features, test_labels,\n",
    "          num_epochs, learning_rate, batch_size, writer=None):\n",
    "    train_ls, test_ls = [], []\n",
    "    \n",
    "    trainset = HousePriceDataset(train_features, train_labels)\n",
    "    trainloader = DataLoader(trainset, batch_size=batch_size,\n",
    "                             shuffle=True, num_workers=4)\n",
    "    \n",
    "    train_features = train_features.to(device)\n",
    "    train_labels = train_labels.to(device)\n",
    "    if test_labels is not None:\n",
    "        test_features = test_features.to(device)\n",
    "        test_labels = test_labels.to(device)\n",
    "    \n",
    "    # The Adam optimization algorithm is used here\n",
    "    optimizer = optim.Adam(net.parameters(), lr=learning_rate)\n",
    "    # optimizer = radam.RAdam(net.parameters(), lr=learning_rate)\n",
    "    \n",
    "    # Iterate over data.\n",
    "    for epoch in range(num_epochs):\n",
    "#         print(f'Epoch {epoch}/{num_epochs - 1}')\n",
    "#         print('-' * 10)\n",
    "\n",
    "        for i, batch in enumerate(trainloader):\n",
    "            inputs, labels = batch\n",
    "            inputs = inputs.to(device)\n",
    "            labels = labels.view(-1, 1)\n",
    "            labels = labels.to(device)\n",
    "            \n",
    "            # zero the parameter gradients\n",
    "            optimizer.zero_grad()\n",
    "            \n",
    "            # forward + backward + optimize\n",
    "            outputs = net(inputs)\n",
    "            loss = criterion(outputs, labels)\n",
    "            loss.backward()\n",
    "            optimizer.step()\n",
    "            \n",
    "            if writer is not None:\n",
    "                writer.add_scalar('training_loss', loss, epoch * len(trainloader) + i)\n",
    "        \n",
    "        # statistics\n",
    "        epoch_loss = log_rmse(net, train_features, train_labels)\n",
    "        train_ls.append(epoch_loss)\n",
    "        if (epoch + 1) % 50 == 0:\n",
    "            print(f'Loss: {epoch_loss}')\n",
    "        \n",
    "        if test_labels is not None:\n",
    "            test_ls.append(log_rmse(net, test_features, test_labels))\n",
    "    \n",
    "    return net, train_ls, test_ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train the network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CUDA is available! Training on GPU ...\n",
      "Loss: 6.492534160614014\n",
      "Loss: 5.800125598907471\n",
      "Loss: 5.395237922668457\n",
      "Loss: 5.1080851554870605\n",
      "Loss: 4.885263442993164\n",
      "Loss: 4.703229904174805\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX4AAAEGCAYAAABiq/5QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAgAElEQVR4nO3deZwdVZ338c+v931fsnVnJ5hgCFlYAgoOIqgogqi4sqjMuIwMszzD85qZR8bX+Iz6uA3KqKCyOICOLIOMgiAKDHsWAtn3rbN0Op30ku5Oevs9f1R1pxPS4aaT23WX7/v1qtetW7dyz69yk985derUKXN3REQkfWREHYCIiIwuJX4RkTSjxC8ikmaU+EVE0owSv4hImsmKOoBYVFVV+aRJk6IOQ0QkqSxZsmSvu1cfvT0pEv+kSZNYvHhx1GGIiCQVM9t6rO3q6hERSTNK/CIiaUaJX0QkzSjxi4ikGSV+EZE0o8QvIpJmlPhFRNJMSif+R15r4D9ePuYwVhGRtJXSif+/X9/FA69uizoMEZGEktKJPz8nk67uvqjDEBFJKKmd+LMz6VTiFxE5Qkon/oKcTDq7e6MOQ0QkoaR04s/PyaKrRy1+EZGhUjrxF+Rk0tPn9PT1Rx2KiEjCSPnED6jVLyIyREon/vyBxK8LvCIig1I68Q+0+DWyR0TksJRO/PnZwQPGNLJHROSw1E786uoREXmTuCV+M/u5me0xsxVDtlWY2VNmtj58LY9X+aCLuyIixxLPFv/dwGVHbbsFeNrdpwNPh+/jJj9bffwiIkeLW+J39+eAfUdtvgK4J1y/B/hQvMqHIS1+JX4RkUGj3cdf6+67wvXdQO1wO5rZjWa22MwWNzU1jaiwgpyBi7tK/CIiAyK7uOvuDvhxPr/D3ee7+/zq6uoRlZE/OJxTo3pERAaMduJvNLOxAOHrnngWpq4eEZE3G+3E/xvg2nD9WuDReBaWnZlBdqbRqVE9IiKD4jmc8wHgJWCGmTWY2WeBbwCXmNl64N3h+7jKy9bDWEREhsqK1xe7+8eH+ejieJV5LAV6CpeIyBFS+s5dCEb2qKtHROSwlE/8+dmZdGlUj4jIoJRP/MHjF9XiFxEZkPKJP1+JX0TkCCmf+ItyszhwSF09IiIDUj7xlxVk09LZE3UYIiIJIw0Sfw6tXd0EM0SIiEjqJ/78bHr6nA7184uIAGmQ+MsLcgBo6eyOOBIRkcSQ8om/rCAbQP38IiKhNEj8QYt/v1r8IiJAGiT+crX4RUSOkPKJv3Qw8avFLyICaZD4y/IHunrU4hcRgTRI/DlZGRTmZKqrR0QklPKJH4ILvOrqEREJpEXiLy/M1qgeEZFQWiT+svwcWrrU1SMiAmmS+MsLc2g+oBa/iAikSeKvLc5lT/tBTdQmIkKaJP4xpXkc7Omn7aDm5RcRSYvEX1OSB8CetoMRRyIiEr20SPy1xbkANLYdijgSEZHopUfiD1v8jWrxi4ikR+KvKQla/LuV+EVE0iPxF+RkUZyXpT5+EREiSvxmdpOZrTCzlWb2V6NRZm1Jnvr4RUSIIPGb2RnA54GzgTOBy81sWrzLrS3JpbFdLX4RkSha/G8DXnH3TnfvBZ4Frop3oWNK8tnVosQvIhJF4l8BvMPMKs2sAHgfUHf0TmZ2o5ktNrPFTU1NJ11oXUU+je0HOdjTd9LfJSKSzEY98bv7auCbwJPAE8Ay4E3Z2N3vcPf57j6/urr6pMutryjAHXa0dJ30d4mIJLNILu66+8/cfZ67vxPYD6yLd5n1FQUAbNvXGe+iREQSWlYUhZpZjbvvMbN6gv79c+NdZl2Y+BuU+EUkzUWS+IGHzKwS6AG+5O4t8S6wuiiX3KwMtfhFJO1Fkvjd/R2jXWZGhlFXUcD2ferjF5H0lhZ37g6oK89nq1r8IpLm0irxT6oqZMveDvr79UAWEUlfaZX4p9cU09XTx85WdfeISPpKq8Q/raYIgA17DkQciYhIdJT4RUTSTFol/orCHCoLc5T4RSStpVXiB5haU8R6JX4RSWNvmfjNrMDM/snM7gzfTzezy+MfWnycVlvEusZ23DWyR0TSUywt/ruAQ8B54fsdwL/ELaI4mzm2lPaDvbqRS0TSViyJf6q7f4tgegXcvROwuEYVR7PGlQCwcmdrxJGIiEQjlsTfbWb5gAOY2VSCM4CkNGNMMZkZxsqdbVGHIiISiVjm6vkqwbz5dWZ2H3A+cF08g4qnvOxMplUXqcUvImnrLRO/uz9lZksJpk424CZ33xv3yOJo1rgS/mfDXtwds6TttRIRGZFhu3rMbO7AAkwEdgE7gfpwW9KaU19GU/shdrbqGbwikn6O1+L/TviaB8wHXido8c8GFnN4lE/SOauuHIDXtu1nfFl+xNGIiIyuYVv87v4ud38XQUt/bvj823nAWQRDOpPW6WOLyc3K4LVtcX/+i4hIwollVM8Md18+8MbdVwBvi19I8ZedmcHsCaUs3bY/6lBEREZdLIn/DTP7qZldFC53Am/EO7B4mzuxnBU7Wuns7o06FBGRURVL4r8eWAncFC6rwm1J7fypVfT0OYu2qNUvIuklluGcB4HvhUvKWDCpgpzMDF7YsJcLT6uOOhwRkVHzlonfzDYT3rU7lLtPiUtEoyQ/J5Oz6st4YUNS35IgInLCYrlzd/6Q9TzgI0BFfMIZXRdMq+K7f1jHvo5uKgpzog5HRGRUvGUfv7s3D1l2uPv3gfePQmxxt3BaFe7w0sbmqEMRERk1sXT1DL1LN4PgDCCWM4WEd+aEUopys3hh417eP3ts1OGIiIyKWBL4d4as9wKbgY/GJ5zRlZWZwTmTK9TPLyJpJZbE/1l33zR0g5lNPplCzexm4HMEF42XA9eHo4dG3UUzqnl6zR7WN7YzvbY4ihBEREZVLOP4H4xxW0zMbDzwFWC+u58BZALXjPT7TtZ7Zo0B4IkVu6MKQURkVA3b4jez04FZQKmZXTXkoxKC0T0nW26+mfUABQSzfkaitiSPufVlPLFyN3958fSowhARGTXHa/HPAC4HyoAPDFnmAp8faYHuvgP4NrCNYAK4Vnd/8uj9zOxGM1tsZoubmppGWlxM3nvGWFbubGNbc2dcyxERSQTHm53zUXe/Hrjc3a8fsnzF3V8caYFmVg5cAUwGxgGFZvapY5R/Rzgj6Pzq6vjeWXtp2N3z+5Xq7hGR1He8B7H8r3D1E2Z229HLSZT5bmCzuze5ew/wMLDwJL7vpNVXFjBzbAmPr9gVZRgiIqPieKN6Voevi09xmduAc82sAOgCLo5DGSfsfW8fw7efXMf2fZ3UVRREHY6ISNwMm/jd/bHw9Z5TWaC7v2JmDwJLCe4LeA2441SWMRJXzp3Ad55ax4NLGrj5ktOiDkdEJG5iuXP3NOBvgUlD93f3Pxtpoe7+VeCrI/3z8TC+LJ8LplXx4JIGbrp4OhkZegi7iKSmWG7g+jXwY+CnQF98w4nW1fMmcNMvl/HypmYWTquKOhwRkbiIJfH3uvuP4h5JArh01hhK8rL4z8XblfhFJGXFcufuY2b2RTMba2YVA0vcI4tAXnYmH5wzjsdX7KalszvqcERE4iKWxH8t8HfAi8CScIl8FE68fOrciRzq7ef+V7dFHYqISFzEMh//5GMsSf30reM5fUwJF0yr4t4Xt9LT1x91OCIip9xbJn4zu+oYy8VmVjMaAUbhhgsmsbvtIL9brhu6RCT1xDQtM3Ae8Kfw/UUE3T2Tzexr7v6LOMUWmYtOq2FKVSE/f34zHzxzHGYa2ikiqSOWPv4s4G3u/mF3/zAwk2Ae/XOAv49ncFHJyDCuv2Ayrze06rGMIpJyYkn8de7eOOT9nnDbPqAnPmFF7yPzJlBbksv3/7Aed486HBGRUyaWxP+Mmf23mV1rZtcCj4bbCoGW+IYXnbzsTL540TRe3bJPrX4RSSmxJP4vAXcDc8LlXuBL7t7h7u+KY2yR+9iCuqDV/7Ra/SKSOmIZzunu/qC73xwuD3qaZMHBVv/mfTyzLr4PgxERGS2xDOc818wWmdkBM+s2sz4zaxuN4BLBx8+uZ2JlAf/6u9X0aly/iKSAWLp6fgh8HFgP5AOfA26PZ1CJJCcrg1suO511jQf49ZKGqMMRETlpsSR+3H0DkOnufe5+F3BZfMNKLJedMYYFk8r5zpPraD+YsgOZRCRNxJL4O80sB1hmZt8ys5tj/HMpw8z4x/fPpLnjEN97an3U4YiInJRYEvingUzgy0AHUAd8OJ5BJaIz68r45Dn13P3iZlbsaI06HBGREYtlVM9Wd+9y9zZ3/2d3/+uw6yft/N2lp1NRmMM//NcK+vrTYmCTiKSgYRO/mb1xvGU0g0wUpfnZ/OP7Z/L69hbuemFz1OGIiIzI8SZp6yeYk+d+4DGga1QiSnBXzBnH75bv4lu/X8s7T6vmtNriqEMSETkhw7b43X0OwTDOIoLk/3VgFrDD3beOTniJx8z4v1e9neLcLG7+1TK6ezW2X0SSy3H7+N19jbt/1d3nErT67wVuHpXIElhVUS5fv/LtrNzZxg//qFE+IpJcjpv4zWy8mf2NmT0PfIog6afFg9ffymVnjOHDcydw+zMbWbRlX9ThiIjE7HgXd58laOVnA9cTPHv3t0BOqj5s/UR99YMzqa8o4Ev3LWVP+8GowxERicnxWvwTgXLgz4HfEzxgfTEp/rD1E1GSl82PPjWXtoM9fPn+1/SMXhFJCse7uDtp6MPVhywp/bD1E3X6mBK+cdVsXt28j28+vibqcERE3tKoT71gZjPMbNmQpc3M/mq04ziVPnTWeD5z3kR++vxmHnlNE7mJSGKL5WHrp5S7ryV4oAtmlgnsAB4Z7ThOtX98/0zWNbbz9w8uZ2xpPudOqYw6JBGRY4p6srWLgY2pcF9ATlYGP/nUfOoq8vnzXyxhY9OBqEMSETmmWB7EMtXMcsP1i8zsK2ZWdorKvwZ4YJhybzSzxWa2uKkpOZ5+VVqQzd3Xn012pnHdXa/S1H4o6pBERN4klhb/Q0CfmU0D7iCYnfP+ky04nOr5g8Cvj/W5u9/h7vPdfX51dfXJFjdq6ioK+Om1C2hqP8Snf/YKLZ3dUYckInKEWBJ/v7v3AlcCP3D3vwPGnoKy3wssdffGU/BdCWVOXRl3fmY+m5o6uPauRRw41Bt1SCIig2JJ/D1m9nGCG7j+O9yWfQrK/jjDdPOkgndMr+b2T85lxY5Wbrh7EV3dfVGHJCICxJb4rwfOA77u7pvNbDLwi5Mp1MwKgUuAh0/mexLdJTNr+d7H5rBoyz6uv/tVtfxFJCHE8iCWVe7+FXd/wMzKgWJ3/+bJFOruHe5e6e4p/yirD545ju9/bA6Ltuzn0z97hdYuPbNXRKIVy6ieZ8ysJJyfZylwp5l9N/6hpY4r5ozn9k8E3T6fuPNlmg9otI+IRCeWrp5Sd28DrgLudfdzgHfHN6zUc9kZY7jzM/PZsOcAH/7Ri2zZ2xF1SCKSpmJJ/FlmNhb4KIcv7soIXDSjhvs/fy6tXT1c9aMXWbJ1f9QhiUgaiiXxf41gds6N7r7IzKYAevrICM2bWM7DXzyfkrwsPnHnyzyxYlfUIYlImonl4u6v3X22u38hfL/J3T8c/9BS1+SqQh76wkJmjivhC/ct5cfPbsTdow5LRNJELBd3J5jZI2a2J1weMrMJoxFcKqssyuWBz5/L+84YyzceX8OXH3iNDg33FJFREEtXz13Ab4Bx4fJYuE1OUl52Jj/8xFnc8t7TeXz5Lq76d130FZH4iyXxV7v7Xe7eGy53A8kzeU6CMzP+4sKp3HPD2TS2H+QDP3yep1al3CwWIpJAYkn8zWb2KTPLDJdPAc3xDizdvGN6NY99+QImVhbw+XsX89VHV3CwR9M8iMipF0viv4FgKOduYBdwNXBdHGNKW3UVBTz0hYXccP5k7nlpKx+6/QXWN7ZHHZaIpJhYRvVsdfcPunu1u9e4+4cAjeqJk9ysTP7PB2Zy13XB1M4f+OHz/OKlLfT3a9SPiJwaI30C11+f0ijkTd51eg2P3/QOFkyq4J8eXcknf/oK25o7ow5LRFLASBO/ndIo5JhqSvK494az+cZVb2f5jlYu/f5z3P3CZrX+ReSkjDTxK/OMEjPjmrPrefLmd3L25ApufWwV19z5Mps17FNERmjYxG9m7WbWdoylnWA8v4yicWX53H39Ar519WxW72rj0u8/x3efWqeRPyJywoZN/O5e7O4lx1iK3T1rNIOUgJnx0fl1/OGvL+SyWWO47en1XPK9Z3l6tcb9i0jsRtrVIxGqLcnjto+fxf2fP4fcrEw+e89iPnfPYrbv08VfEXlrSvxJbOHUKn73lXdwy3tP58WNe7n4u8/yr4+v1lO+ROS4lPiTXE5WBn9x4VSe/psL+cDscdzx3CYu+n9/4u4XNtPd2x91eCKSgJT4U8TY0ny+89EzeezLF/C2sSXc+tgq3vO9Z3l8+S5N+SwiR1DiTzFnjC/lvs+dw13XLSA7M4Mv3LeUy3/wPH9Y1agKQEQAJf6UZGaDd/5+5yNn0n6wl8/du5gP3f4Cz6zdowpAJM1ZMiSB+fPn++LFi6MOI2n19PXz8NIGbnt6AztauphbX8bNl5zGBdOqMNNN2CKpysyWuPv8N21X4k8f3b39/HrJdn74xw3saj3I7Aml/MWFU7l01hgyM1QBiKQaJX4ZdKi3j4eX7uAnz25kS3Mnk6sKufGdU7hq7nhyszKjDk9EThElfnmTvn7niRW7+fGzG1m+o5Wa4lxuuGAyH19QT2lBdtThichJSqjEb2ZlwE+BMwgmfLvB3V8abn8l/vhyd17Y0MyPnt3ACxuayc/O5Mq547lu4SROqy2OOjwRGaHhEn9Uc+78G/CEu19tZjlAQURxCMEooAumV3HB9CpW7Wzjnhe38NCSBu5/ZRvnT6vkuoWT+bPTa3QdQCRFjHqL38xKgWXAFI+xcLX4R9++jm5+uWgbv3hpK7taD1JXkc9nzp3E1fMmUF6YE3V4IhKDhOnqMbM5wB3AKuBMYAlwk7t3HLXfjcCNAPX19fO2bt06qnFKoLevnydXNXL3i1t4dfM+cjIzuOyMMVyzoI5zp1SSobMAkYSVSIl/PvAycL67v2Jm/wa0ufs/Dfdn1OJPDGt2t/HLV7fz8NIG2g72MrGygI8tqOPqeROoKc6LOjwROUoiJf4xwMvuPil8/w7gFnd//3B/Rok/sRzs6ePxFbt44NXtvLp5H5kZxsWn13D1vAlcNKOGnCzdEC6SCBLm4q677zaz7WY2w93XAhcTdPtIksjLzuTKsyZw5VkT2Nh0gF8t2s5DSxp4clUj5QXZXD57HFfOHc9ZdWW6M1gkAUU1nHMOwXDOHGATcL277x9uf7X4E19PXz//s76Jh5fu4KlVjRzq7WdyVSEfmjOeK88aT32lBm6JjLaE6eoZCSX+5NJ2sIcnlu/m4dcaeHnTPgDmTyzng3PGcdkZY3Q9QGSUKPFLJHa0dPHosh3812s7WNd4ADM4e1IFl88ey6WqBETiSolfIreusZ3fvrGL3y7fxYY9QSVwzuQK3v92VQIi8aDELwnlWJXAgokVXDKzlktm1jKpqjDqEEWSnhK/JCR3Z13jAX67fBdPrtzNmt3tAEyvKeI9s2q5ZOYYZo8v1Y1iIiOgxC9JYfu+Tp5a1ciTq3azaMt++vqdmuJc3h2eCSycWqmpo0VipMQvSaels5s/rtnDH1Y38uzaJjq6+yjIyWTh1EouPK2ai2bUUFehYaIiw1Hil6R2sKePlzY186c1e3hmbRPb9nUCMKWqkAtnBJXAOZMryMvW2YDIACV+SRnuzua9HTyztoln1zXx8qZmDvX2k5edwblTKrnotGounFHDpMoC3TksaU2JX1LWwZ4+Xt7UPFgRbN4bTPQ6viyf86ZWcv60ShZOraK2RMNFJb0kzFw9IqdaXnYmF82o4aIZNQBsbe7gufV7eXHDXv6wupEHlzQAMK2miIVTg0rgvCmVerykpC21+CWl9fc7q3a18eLGvbywoZlXN++jq6ePDIMzxpeycGoVC6dWMm9iOYW5agdJalFXjwjQ3dvP6w0tvLBhLy9uaOa17fvp6XMyM4wzxpVw9uQKzp5cyYJJ5ZQV6EljktyU+EWOobO7l0Vb9rNo8z5e3byPZQ0tdPf2AzCjtpizJ1ewYHIFZ0+qYEyprhFIclHiF4nBwZ4+3mhoZdGWfbyyeR9Ltuyjo7sPgPqKAs6eXMG8ieXMrS9nek2R7iiWhKbELzICvX39rN7Vziubg+sDi7bsY39nDwDFuVnMqS/jrPpy5oavpfm6YCyJQ4lf5BQYuIdg6bYWlm7bz9Kt+1nX2E5/+N9oWk0R8+rLmTuxjLn15Uyt1lmBREeJXyRODhzq5fXtLSzdup+l2/bz2vYWWsKzgpK8LM6sK+Pt40uZPaGMM+tKGVOSpxvLZFRoHL9InBTlZnH+tCrOn1YFBGcFm/Z2hBVBC280tHDHc5voDU8LqopyOXNCUBHMnlDK7AmlVBblRnkIkmaU+EVOMTNjanURU6uL+Mj8OiC4aLx6VxtvNLTyekMLbzS08se1exg44R5flh9WAkFlcMb4Ul0vkLhR4hcZBXnZmZxVX85Z9eWD2w4c6mXFjlbeCCuCNxpaeXzF7sHP6yrymTW2lJnjSpg1roRZ40qpLclVN5GcNCV+kYgU5WZx7pRKzp1SObhtf0c3b+xoZcWOVlbtbGPVrjaeWHm4MqgszGHmuJKwMihl5tgSJlcVkqkLyHIClPhFEkh5YQ4XnlbNhadVD247cKiX1bvaWLWzjZU7W1m5s42fP7+Znr6gn6ggJ5PTxxQHFcG4EmaMKWZGbbGmoJBhaVSPSBLq7u1nw54DrNzZyqpdbazc2cbqnW20H+od3KeuIp8ZtSWcPqaYGWOKOX1MMZOrCsnKzIgwchlNGtUjkkJysjIGu3wG9Pc7O1q6WLO7nTW72ljT2M7a3e38ae0e+sIRRTmZGUytKRqsDAYqBA0xTS9K/CIpIiPDqKsooK6igEtm1g5uP9jTx8amA6zdHVQEa3a389LGZh55bcfgPqX52cyoLWZ6bRHTa4qYXlvMtJoiaop1MTkVRZL4zWwL0A70Ab3HOhURkVMjLzuTWeNKmTWu9IjtLZ3dgxXBmt3trN3dxm9e30n7wcPdRcV5WUyvKWJaTRHTa4qZFlYM40rzdUdyEouyxf8ud98bYfkiaa2sIIdzplRyzpBRRe5OU/sh1u85wIY9B1i/p50New7wxzV7+M/FDYP75WdnhpVBEVNrDp8l1JXn6xpCElBXj4gMMjNqSvKoKckbvBN5wP6ObjY0HWB94+EK4aVNzTw8pMsoJzODyVWFwVJdyJSqQqZUFzK5qoiKQj3fIFFElfgdeNLMHPiJu98RURwiEqPywhwWFFawYFLFEdvbD/awsamD9Y1BZbCxqYP1e9p5ek3j4JBTgLKC7MFKYWp10eEKoqqQvOzM0T6ctBbJcE4zG+/uO8ysBngK+Et3f+6ofW4EbgSor6+ft3Xr1lGPU0RGrrevn4b9XWze28GmvR1sajoQrDd1sLvt4BH7ji/LZ/Lg2cHhymFcWb5uTjsJCTs7p5ndChxw928Pt4/G8Yuklo5DvWxpDiqBzXs7wgrhAJuaOo64FyEnM4MJFflMrChgYmUh9RUFTKoqoL6ikLqKfHKzdKZwPAkzjt/MCoEMd28P198DfG204xCR6BTmZh1zpJG709zRHVYIB9i0t4NtzZ1sbe5k0Zb9HBhSKZjBuNJ86isKmFhZQH1lAZPCymFiZQHFeZrkbjhR9PHXAo+EY4OzgPvd/YkI4hCRBGNmVBXlUlWUy9mTj7yWMFApbG3uZNu+Drbs7WTbvk62Nnfw1KpGmju6j9i/sjCH+soCJlYUUF9ZyKTKoEKoKy+gOs3vTxj1xO/um4AzR7tcEUluQyuFeRPL3/R5+8GesCLoPKJyWLRlP4++vpOhvdq5WRmML8+nrryAuop8JpQXHLFeXpCd0hWDhnOKSEoozss+ZvcRwKHePhr2d7G1uYOG/V1s39cZvO7vZNn2Flq7eo7YvzAnM6gMwopgQnl+cFd0eQETKvIpSfJuJCV+EUl5uVmZgw/HOZa2gz007OuiYX8n24dUDA37O3lpYzMd3X1H7F+anx1UBmHlML4sn/HlBYwry2N8WT6l+Yl9xqDELyJpryQvm5njso+Y9G6Au9PS2cP2/Z1vOltYvyeYBO9Qb/8Rf6YgJ5NxZfmMKwsrhbK8I96PKc0jO8I7nJX4RUSOw8woL8yhvDCH2RPK3vT5wEXnnS1d7GzpomF/FztbDgbvW7tYuaP1TReezaC2OI9xYYUwfkilMPBakp8Vt7MGJX4RkZMw9KLzsSoGCGZIDSqGoEJoCCuJnS1drNjRypMrG+nuO/KsoTA8a/jJp+cxZZguqpFS4hcRibO87EymVBcNm8D7+529HYcOnym0dLGjpYsd+7sozT/1F5KV+EVEIpaRYdQU51FTnMecumOfNZzS8uJegoiIJBQlfhGRNKPELyKSZpT4RUTSjBK/iEiaUeIXEUkzSvwiImlGiV9EJM1E/ujFWJhZEzDSh+5WAXtPYThR0rEkJh1LYkqVYzmZ45jo7tVHb0yKxH8yzGzxsZ45mYx0LIlJx5KYUuVY4nEc6uoREUkzSvwiImkmHRL/HVEHcArpWBKTjiUxpcqxnPLjSPk+fhEROVI6tPhFRGQIJX4RkTST0onfzC4zs7VmtsHMbok6nhNhZlvMbLmZLTOzxeG2CjN7yszWh6/lUcc5HDP7uZntMbMVQ7YdM34L3Bb+Tm+Y2dzoIj/SMMdxq5ntCH+bZWb2viGf/e/wONaa2aXRRH1sZlZnZn8ys1VmttLMbgq3J+PvMtyxJN1vY2Z5Zvaqmb0eHss/h9snm9krYcy/MrOccHtu+H5D+PmkEy7U3VNyATKBjcAUIAd4HZgZdVwnEP8WoOqobd8CbgnXbwG+GXWcx4n/ncBcYMVbxQ+8D3gcMOBc4JWo43+L47gV+Ntj7Dsz/HeWC0wO//1lRn0MQ+IbC8wN14uBdWHMyfi7DHcsSffbhH+/RcyW/NEAAAXISURBVOF6NvBK+Pf9n8A14fYfA18I178I/Dhcvwb41YmWmcot/rOBDe6+yd27gV8CV0Qc08m6ArgnXL8H+FCEsRyXuz8H7Dtq83DxXwHc64GXgTIzGzs6kR7fMMcxnCuAX7r7IXffDGwg+HeYENx9l7svDdfbgdXAeJLzdxnuWIaTsL9N+Pd7IHybHS4O/BnwYLj96N9l4Pd6ELjYzOxEykzlxD8e2D7kfQPH/4eRaBx40syWmNmN4bZad98Vru8GaqMJbcSGiz8Zf6svh90fPx/S5ZY0xxF2D5xF0LpM6t/lqGOBJPxtzCzTzJYBe4CnCM5IWty9N9xlaLyDxxJ+3gpUnkh5qZz4k90F7j4XeC/wJTN759APPTjPS9qxuEke/4+AqcAcYBfwnWjDOTFmVgQ8BPyVu7cN/SzZfpdjHEtS/jbu3ufuc4AJBGcip8ezvFRO/DuAuiHvJ4TbkoK77whf9wCPEPxjaBw41Q5f90QX4YgMF39S/Vbu3hj+R+0H7uRwl0HCH4eZZRMkyvvc/eFwc1L+Lsc6lmT+bQDcvQX4E3AeQddaVvjR0HgHjyX8vBRoPpFyUjnxLwKmh1fGcwgugvwm4phiYmaFZlY8sA68B1hBEP+14W7XAo9GE+GIDRf/b4DPhKNIzgVah3Q9JJyj+rmvJPhtIDiOa8JRF5OB6cCrox3fcMJ+4J8Bq939u0M+SrrfZbhjScbfxsyqzawsXM8HLiG4ZvEn4Opwt6N/l4Hf62rgj+GZWuyivqIdz4VgVMI6gv6yf4g6nhOIewrBCITXgZUDsRP04z0NrAf+AFREHetxjuEBglPtHoL+yc8OFz/BqIbbw99pOTA/6vjf4jh+Ecb5RvifcOyQ/f8hPI61wHujjv+oY7mAoBvnDWBZuLwvSX+X4Y4l6X4bYDbwWhjzCuD/hNunEFROG4BfA7nh9rzw/Ybw8yknWqambBARSTOp3NUjIiLHoMQvIpJmlPhFRNKMEr+ISJpR4hcRSTNK/JJUzKxvyMyLy+wUzrpqZpOGzsJ5nP1uNbNOM6sZsu3A8f7MqY5B5GRkvfUuIgmly4Nb26O2F/gb4O+jDmQoM8vyw/O7iByTWvySEix4fsG3LHiGwatmNi3cPsnM/hhO2vW0mdWH22vN7JFwDvTXzWxh+FWZZnZnOC/6k+GdlMfyc+BjZlZxVBxHtNjN7G/N7NZw/Rkz+56ZLTaz1Wa2wMwetmAe/H8Z8jVZZnZfuM+DZlYQ/vl5ZvZsOHHf74dMs/CMmX3fguc23HTyf5uS6pT4JdnkH9XV87Ehn7W6+9uBHwLfD7f9ALjH3WcD9wG3hdtvA5519zMJ5ttfGW6fDtzu7rOAFuDDw8RxgCD5n2ii7Xb3+QTzqz8KfAk4A7jOzAZmWJwB/Lu7vw1oA74YzkvzA+Bqd58Xlv31Id+b4+7z3T0pJiWTaKmrR5LN8bp6Hhjy+r1w/TzgqnD9FwQPHYFgrvPPQDAzItAaTuG72d2XhfssASYdJ5bbgGVm9u0TiH9gvqjlwEoP574xs00EE2+1ANvd/YVwv/8AvgI8QVBBPBVOvZ5JMJXEgF+dQAyS5pT4JZX4MOsn4tCQ9T5guK4e3L3FzO4naLUP6OXIM+m8Yb6//6iy+jn8//Ho2J1g3pyV7n7eMOF0DBenyNHU1SOp5GNDXl8K118kmJkV4JPA/4TrTwNfgMGHYJSOsMzvAn/O4aTdCNSYWaWZ5QKXj+A7681sIMF/AnieYGKx6oHtZpZtZrNGGLOkOSV+STZH9/F/Y8hn5Wb2BkG/+83htr8Erg+3f5rDffI3Ae8ys+UEXTozRxKMu+8leF5Cbvi+B/gawayJTwFrRvC1awkevrMaKAd+5MHjQ68GvmlmrxPMRrnwON8hMizNzikpwcy2EEwbvDfqWEQSnVr8IiJpRi1+EZE0oxa/iEiaUeIXEUkzSvwiImlGiV9EJM0o8YuIpJn/D+PzTTfVNMQVAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "EPOCHS = 300\n",
    "BATCH_SIZE = 128\n",
    "LEARNING_RATE = 0.01\n",
    "\n",
    "# TensorBoard setup\n",
    "tb_logdir = '/workspace/ml/tensorboard-logs'\n",
    "os.system(f'rm {tb_logdir}/*')\n",
    "writer = SummaryWriter(tb_logdir)\n",
    "\n",
    "# check if CUDA is available\n",
    "train_on_gpu = torch.cuda.is_available()\n",
    "device = torch.device('cuda:0' if train_on_gpu else 'cpu')\n",
    "if not train_on_gpu:\n",
    "    print('CUDA is not available. Training on CPU ...')\n",
    "else:\n",
    "    print('CUDA is available! Training on GPU ...')\n",
    "\n",
    "# Model allocation\n",
    "net = get_net()\n",
    "net.to(device)\n",
    "\n",
    "net, train_ls, test_ls = train(net, train_features, train_labels, test_features, test_labels=None,\n",
    "                               num_epochs=EPOCHS, learning_rate=LEARNING_RATE, batch_size=BATCH_SIZE, writer=writer)\n",
    "\n",
    "# writer.close()\n",
    "plt.xlabel('Epoch Number')\n",
    "plt.ylabel(\"Loss Magnitude\")\n",
    "plt.plot(list(range(EPOCHS)), train_ls)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
