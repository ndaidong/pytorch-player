# To play with Pytorch


# Setup


```bash
git clone git@gitlab.com:ndaidong/pytorch-player.git
cd pytorch-player
python3 -m venv venv
source venv/bin/activate
(venv) pip install -r requirements.txt

# set environment variables:
(venv) export DATA_FILE=/path/to/your/data_file
(venv) export OUTPUT_DIR=/path/to/your/output_dir

# and train
python train.py
```


