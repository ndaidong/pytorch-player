#!/usr/bin/env python3

import os
import time
from datetime import datetime
from copy import deepcopy

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision
import torchvision.transforms as transforms

import matplotlib.pyplot as plt


DATA_DIR = os.getenv('DATA_DIR', '/workspace/ml/datasets')
OUTPUT_DIR = os.getenv('OUTPUT_DIR', '/workspace/ml/output')
MODEL_NAME = os.getenv('MODEL_NAME', 'cifar10')


class NeuralNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


def get_time():
    return int(time.time())


def train(learning_rate=0.001, epochs=50):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'Start training on {device}...')

    model = NeuralNet()
    model.to(device)
    print('Model architecture:')
    print(model)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)

    transform = transforms.Compose([
        transforms.Resize((32, 32)),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    trainset = torchvision.datasets.CIFAR10(
        root=DATA_DIR,
        train=True,
        download=False,
        transform=transform
    )
    trainloader = torch.utils.data.DataLoader(
        trainset,
        batch_size=32,
        shuffle=True,
        num_workers=2
    )
    print('Classes:')
    print(trainset.class_to_idx)

    epoch_list = []
    epoch_loss_list = []
    log_list = []

    best_model_wht = deepcopy(model.state_dict())
    best_loss = 1.0

    start = datetime.fromtimestamp(get_time())

    for epoch in range(epochs):
        epoch_list.append(epoch)
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

        epoch_loss = running_loss / len(trainset)
        epoch_loss_list.append(epoch_loss)

        if epoch_loss < best_loss:
            best_loss = epoch_loss
            best_model_wht = deepcopy(model.state_dict())

        msg = f'Epoch: {epoch + 1} Loss: {epoch_loss}'
        print(msg)
        log_list.append(msg)

    now = get_time()
    end = datetime.fromtimestamp(now)
    print(f'Training completed in {end - start}')

    mfile = f'{OUTPUT_DIR}/{MODEL_NAME}_checkpoint_{now}.pth'
    torch.save(best_model_wht, mfile)

    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.plot(epoch_list, epoch_loss_list)
    pfile = f'{OUTPUT_DIR}/{MODEL_NAME}_plot_{now}.png'
    plt.savefig(pfile)

    lfile = f'{OUTPUT_DIR}/{MODEL_NAME}_log_{now}.txt'
    with open(lfile, 'w') as fo:
        fo.write('\n'.join(log_list))


def init():
    if not os.path.exists(DATA_DIR):
        print('DATA_DIR is wrong or not set')
        exit()
    if not os.path.exists(OUTPUT_DIR):
        os.system(f'mkdir -p "{OUTPUT_DIR}"')
    train()


if __name__ == '__main__':
    init()
