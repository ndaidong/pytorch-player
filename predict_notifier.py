#!/usr/bin/env python3

import sys
import random

import torch

from train import NeuralNet


def predict(model_path):
    try:
        model = NeuralNet()
        model.load_state_dict(torch.load(model_path))
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        model.to(device)
        for k in range(10):
            priority = random.randint(0, 3)
            frequency = random.randint(0, 5)
            duration = random.randint(0, 60)

            sample = torch.tensor([
                priority,
                frequency,
                duration
            ], dtype=torch.float32)
            sample = sample.view(-1, 3)
            prediction = model(sample.to(device))
            print(f'model({priority}, {frequency}, {duration}) = {prediction}')

    except Exception as err:
        print(err)


def init():
    argv = sys.argv
    if len(argv) == 2:
        predict(argv[1])


if __name__ == '__main__':
    init()
