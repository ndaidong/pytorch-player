#!/usr/bin/env python3

import os
import random

import pandas as pd

DATA_FILE = os.getenv('DATA_FILE', 'samples.csv')


def build_data(count=100):
    arr = []
    while len(arr) < count:
        priority = random.randint(0, 3)
        frequency = random.randint(0, 5)
        duration = random.randint(0, 60)
        label = 0
        if (priority == 3 or frequency > 2) and duration > 30:
            label = 1

        sample = [
            priority,
            frequency,
            duration,
            label
        ]
        arr.append(sample)
    return arr


def make():
    if not os.path.exists(DATA_FILE):
        samples = build_data(10000)
        headers = [
            'Priority',
            'Frequency',
            'Duration',
            'Label'
        ]
        df = pd.DataFrame(samples, columns=headers)
        df.to_csv(DATA_FILE, index=False)
        print('Dataset has been generated')
    else:
        print('Dataset is already generated')


if __name__ == '__main__':
    make()

