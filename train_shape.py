#!/usr/bin/env python3

import os
import time
from datetime import datetime
from copy import deepcopy

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision
import torchvision.transforms as transforms

import matplotlib.pyplot as plt


DATA_DIR = os.getenv('DATA_DIR', '/workspace/ml/datasets/')
OUTPUT_DIR = os.getenv('OUTPUT_DIR', '/workspace/ml/output')
MODEL_NAME = os.getenv('MODEL_NAME', 'shape_detector')


def train():
    pass


def init():
    if not os.path.exists(DATA_DIR):
        print('DATA_DIR is wrong or not set')
        exit()
    if not os.path.exists(OUTPUT_DIR):
        os.system(f'mkdir -p "{OUTPUT_DIR}"')

    train()


if __name__ == '__main__':
    init()
